//
//  AppDelegate.m
//  SmartPlugEmulator
//
//  Created by Ivan Khvorostinin on 17/05/2017.
//  Copyright © 2017 IX Geeks. All rights reserved.
//

#import <CocoaAsyncSocket/GCDAsyncSocket.h>

#import "AppDelegate.h"
#import "Config.h"

@interface AppDelegate () <GCDAsyncSocketDelegate>

@property GCDAsyncSocket * socket;
@property NSMutableArray * sockets;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSError * error;
    
    self.sockets = NSMutableArray.new;
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self
                                             delegateQueue:dispatch_get_main_queue()];
    
    BOOL result = [self.socket acceptOnPort:kPort
                                      error:&error];
    
    if (!result || error)
    {
        NSLog(@"Failed to accept on port [%d]: %@", kPort, error);
    }
    else
    {
        NSLog(@"Accepted on port [%d]", kPort);
    }
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
    NSLog(@"Accepted new socket");
    
    [self.sockets addObject:newSocket];
    
    [newSocket writeData:[@"#203:32:995:161:1:0:0:0" dataUsingEncoding:NSUTF8StringEncoding]
             withTimeout:1
                     tag:0];
    
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag;
{
    NSLog(@"Data written");
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
    return 1;
}

@end
