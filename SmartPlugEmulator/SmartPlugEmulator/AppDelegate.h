//
//  AppDelegate.h
//  SmartPlugEmulator
//
//  Created by Ivan Khvorostinin on 17/05/2017.
//  Copyright © 2017 IX Geeks. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

