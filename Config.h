//
//  Config.h
//  SmartPlugEmulator
//
//  Created by Ivan Khvorostinin on 17/05/2017.
//  Copyright © 2017 IX Geeks. All rights reserved.
//

#pragma once

static NSString * kHost = @"192.168.4.1";
static const int  kPort = 85;

//static NSString * kHost = @"localhost";
//static const int  kPort = 8500;

//static NSString * kHost = @"192.168.0.49";
//static const int  kPort = 8500;
