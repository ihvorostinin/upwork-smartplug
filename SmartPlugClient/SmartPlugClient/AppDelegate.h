//
//  AppDelegate.h
//  SmartPlugClient
//
//  Created by Ivan Khvorostinin on 17/05/2017.
//  Copyright © 2017 IX Geeks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

