//
//  AppDelegate.m
//  SmartPlugClient
//
//  Created by Ivan Khvorostinin on 17/05/2017.
//  Copyright © 2017 IX Geeks. All rights reserved.
//

#import <CocoaAsyncSocket/GCDAsyncSocket.h>
#import <SystemConfiguration/CaptiveNetwork.h>

#import "AppDelegate.h"
#import "Config.h"

id fetchSSIDInfo()
{
    NSArray *ifs = (NSArray *)CFBridgingRelease(CNCopySupportedInterfaces());
    NSLog(@"Supported interfaces: %@", ifs);
    NSDictionary *info;
    for (NSString *ifnam in ifs) {
        info = (NSDictionary *)CFBridgingRelease(CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam));
        NSLog(@"%@ => %@", ifnam, info);
        if (info && [info count]) { break; }
    }
    return info;
}


@interface AppDelegate () <GCDAsyncSocketDelegate>

@property GCDAsyncSocket * socket;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSError * error;
        
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self
                                             delegateQueue:dispatch_get_main_queue()];
    
    BOOL result = [self.socket connectToHost:kHost onPort:kPort withTimeout:1 error:&error];
    
    if (!result || error)
    {
        NSLog(@"Failed to connect to [%@:%d]: %@", kHost, kPort, error);
    }
    else
    {
        NSLog(@"Connecting to [%@:%d]", kHost, kPort);
    }
    
    NSLog(@"Network info %@", fetchSSIDInfo());
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
    NSLog(@"Connected to host [%@] on port [%d]", host, port);
    
    [sock readDataWithTimeout:1 tag:0];
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag;
{
    NSLog(@"Received data [%@]", [NSString stringWithCString:data.bytes encoding:NSUTF8StringEncoding]);
}

@end
