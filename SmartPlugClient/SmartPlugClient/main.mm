//
//  main.m
//  SmartPlugClient
//
//  Created by Ivan Khvorostinin on 17/05/2017.
//  Copyright © 2017 IX Geeks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
